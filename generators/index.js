var Generator = require('yeoman-generator');
var mkdirp = require('mkdirp');

module.exports = class extends Generator{

async prompting() {
    this.answers = await this.prompt([{
      type    : 'input',
      name    : 'app_name',
      message : 'Your project name',
      default: this.appname
    },
    {
      type  :  'input',
      name  :  'database_name',
      message:  'Mongo database name:',
    }
    
    ]);
  }
  createDirectories(){
    //app_api
    mkdirp.sync('./app_api/config');
    mkdirp.sync('./app_api/controllers');
    mkdirp.sync('./app_api/models');
    mkdirp.sync('./app_api/routes');

   mkdirp.sync('./public/stylesheets');
//    mkdirp.sync('./'+this.answers.app_name+'/app_server/routes');
//    mkdirp.sync('./'+this.answers.app_name+'/app_server/views');

    //app_client
    mkdirp.sync('./app_client/views');
    mkdirp.sync('./app_client/controllers');
    mkdirp.sync('./app_client/common/directives');
    mkdirp.sync('./app_client/common/filters');
    mkdirp.sync('./app_client/common/factories');
    mkdirp.sync('./app_client/common/services');
    






  }
  

  writingTemplateFiles() {
    // this.fs.copyTpl(
    //   this.templatePath(''),
    //   this.destinationPath(''),
    //   {  } 
    // );
    
    //package.json
    this.fs.copyTpl(
      this.templatePath('_package.json'),
      this.destinationPath('package.json'),
      { name: this.answers.app_name }
    );
    // .env uncomment later
    this.fs.copyTpl(
      this.templatePath('_.env'),
      this.destinationPath('./.env'),
      { database_name: this.answers.database_name }  
    );
    //server.js
    this.fs.copyTpl(
      this.templatePath('_server.js'),
      this.destinationPath('server.js'),
      { app_name: this.answers.app_name } 

    );
    //nodemon.json - to stop nodemon from continuous auto restart
    this.fs.copy(
      this.templatePath('_nodemon.json'),
      this.destinationPath('nodemon.json')
    );
    //stylesheet
    this.fs.copy(
      this.templatePath('_public/_stylesheets/_style.css'),
      this.destinationPath('public/stylesheets/style.css')
    );

    //app_client/index.html
    this.fs.copyTpl(
    this.templatePath('./_app_client/_index.html'),
    this.destinationPath('./app_client/index.html'),
    { app_name: this.answers.app_name } 
    );

    //app_client/routes.js
    this.fs.copyTpl(
    this.templatePath('./_app_client/_routes.js'),
    this.destinationPath('app_client/routes.js'),
    { app_name: this.answers.app_name } 
    );

    //navigation bar directive/navigation
    this.fs.copyTpl(
    this.templatePath('_app_client/_common/_directives/_navigation/_navigation.controller.js'),
    this.destinationPath('app_client/common/directives/navigation/client.navigation.controller.js'),
    { app_name: this.answers.app_name  } 
    );
    this.fs.copyTpl(
    this.templatePath('_app_client/_common/_directives/_navigation/_navigation.directive.js'),
    this.destinationPath('app_client/common/directives/navigation/client.navigation.directive.js'),
    { app_name: this.answers.app_name  } 
    );
    this.fs.copyTpl(
    this.templatePath('_app_client/_common/_directives/_navigation/_navigation.template.view.html'),
    this.destinationPath('app_client/common/directives/navigation/client.navigation.template.view.html'),
    { app_name: this.answers.app_name  } 
    );

    //app_client/views
    this.fs.copyTpl(
    this.templatePath('_app_client/_views/_home.view.html'),
    this.destinationPath('app_client/views/home.view.html'),
    { app_name: this.answers.app_name  } 
    );
    this.fs.copy(
    this.templatePath('_app_client/_views/_page1.view.html'),
    this.destinationPath('app_client/views/page1.view.html')
    );
    this.fs.copy(
    this.templatePath('_app_client/_views/_page2.view.html'),
    this.destinationPath('app_client/views/page2.view.html')
    );
    this.fs.copy(
    this.templatePath('_app_client/_views/_page3.view.html'),
    this.destinationPath('app_client/views/page3.view.html')
    );
    this.fs.copyTpl(
    this.templatePath('_app_client/_controllers/_home.controller.js'),
    this.destinationPath('app_client/controllers/client.home.controller.js'),
    { app_name: this.answers.app_name  } 
    );

    //mongoDB database
    this.fs.copyTpl(
    this.templatePath('_app_api/_models/_db_connection.js'),
    this.destinationPath('app_api/models/api.db_connection.js'),
    { database_name: this.answers.database_name } 
    );
    this.fs.copy(
    this.templatePath('_app_api/_models/_db_schemas.js'),
    this.destinationPath('app_api/models/api.db_schemas.js')
    );

    //passport authentication
    this.fs.copy(
    this.templatePath('_app_api/_config/_passport.js'),
    this.destinationPath('app_api/config/api.passport.js')
    );
    this.fs.copy(
    this.templatePath('_app_api/_controllers/_authentication.js'),
    this.destinationPath('app_api/controllers/api.authentication.js')
    );
    this.fs.copyTpl(
    this.templatePath('_app_client/_common/_services/_authentication.service.js'),
    this.destinationPath('app_client/common/services/client.authentication.service.js'),
    { app_name: this.answers.app_name  } 
    );


    //API routes
    this.fs.copy(
    this.templatePath('_app_api/_routes/_index.js'),
    this.destinationPath('app_api/routes/api.index.js')
    );

    //API controllers
    this.fs.copy(
    this.templatePath('_app_api/_controllers/_home.js'),
    this.destinationPath('app_api/controllers/api.home.js')
    );

    this.fs.copyTpl(
    this.templatePath('_app_client/_controllers/_authentication.controller.js'),
    this.destinationPath('app_client/controllers/client.authentication.controller.js'),
    { app_name: this.answers.app_name  } 
   );

    //HTML Line Breaks filter
     this.fs.copyTpl(
     this.templatePath('_app_client/_common/_filters/_addHtmlLineBreaks.js'),
     this.destinationPath('app_client/common/filters/client.addHtmlLineBreaks.filter.js'),
     { app_name: this.answers.app_name  }
    );

  }
	
};
