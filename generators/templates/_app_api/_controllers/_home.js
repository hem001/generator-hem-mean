var mongoose = require('mongoose');
var ObjectId = require('mongodb').ObjectId;
var User = mongoose.model('User');

var sendJsonResponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.getUsers = function(req, res) {
sendJsonResponse(res, 200, "here is a list of users");
};
