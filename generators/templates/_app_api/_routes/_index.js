var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'payload'
});

var ctrlAuth = require('../controllers/api.authentication');
var ctrlHome = require('../controllers/api.home');

router.get('/getUsers', ctrlHome.getUsers);


//user authentication
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);




module.exports = router;
