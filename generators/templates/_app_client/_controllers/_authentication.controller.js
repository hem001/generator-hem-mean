(function() {
  angular
    .module('<%= app_name%>')
    .controller('loginCtrl', loginCtrl);

  loginCtrl.$inject = ['$location', 'authenticationService'];

  function loginCtrl($location, authenticationService) {
    var vm = this;
    this.title = "Home Page";
    // console.log('help me ');

    // vm.credentials = {
    //   email: '',
    //   password: ''
    // };

    vm.returnPage = $location.search().page || '/';
    vm.formError = '';

    vm.login = function() {
      // console.log("hello");
      
      if (!vm.credentials || !vm.credentials.name ||!vm.credentials.email || !vm.credentials.password) {
        vm.formError = "All fields required, please try again";
        return false;
      } else {
        vm.formError = "";
        authenticationService
          .login(vm.credentials)
          .then(function(result) {

            if (result.status === 200) {
              // console.log('here', result.status);
              alert('login successful');

              $location.path(vm.returnPage);
              // window.location.href= '/';
            }else{

              vm.formError = 'wrong email or password';
              // alert('wrong username and password');
            }
          });

      }
    };

    vm.register = function() {
      vm.isLoggedIn = authenticationService.isLoggedIn();
      // vm.isLoggedIn = true;
      if (!vm.credentials || !vm.credentials.name || !vm.credentials.email || !vm.credentials.password) {
        vm.formError = "All fields required, please try again";
        return false;
      } else {
        //Register

        vm.formError = '';
        if (vm.isLoggedIn) {
          authenticationService
            .register(vm.credentials)
            .then(function(result) {
              if (result.status === 200) {
                alert('Successfully registered a new user');

                $location.path(vm.returnPage);
              }else{

                vm.formError = 'oops something went wrong';
              }
            });
        }else{
          alert("You my friend are not logged in!!!");
        }

      }

    };


  }
})();
