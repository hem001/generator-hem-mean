(function() {
  angular
    .module('<%= app_name%>')
    .service('authenticationService', authenticationService);

  authenticationService.$inject = ['$window', '$http'];

  function authenticationService($window, $http) {

    var saveToken = function(token) {
      $window.localStorage['<%= app_name%>-token'] = token;
    };

    var getToken = function() {
      return $window.localStorage['<%= app_name%>-token'];
    };

    var register = function(user) {

      return $http.post('/api/register', user)
      .then(function(res) {

        saveToken(res.data.token);
        return res;

      }, function(e) {
        return e;
      });
    };

    var login = function(user) {
      return $http.post('/api/login', user)
      .then((function(res) {

        saveToken(res.data.token);
        return res;
      }),(function(e) { 
        return e; 
      }
        ));
    };

    var logout = function() {
      $window.localStorage.removeItem('<%= app_name%>-token');
    };

    var isLoggedIn = function() {
      var token = getToken();

      if (token) {
        var payload = JSON.parse($window.atob(token.split('.')[1]));

        return payload.exp > Date.now() / 1000;
      } else {
        return false;
      }
    };

    var currentUser = function() {
      if (isLoggedIn()) {
        var token = getToken();
        var payload = JSON.parse($window.atob(token.split('.')[1]));
        return {
          email: payload.email,
          name: payload.name
        };
      }
    };





    return {
      saveToken: saveToken,
      getToken: getToken,
      register: register,
      login: login,
      logout: logout,
      isLoggedIn: isLoggedIn,
      currentUser: currentUser

    };
  }
})();