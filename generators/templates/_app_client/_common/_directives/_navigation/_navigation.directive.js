(function(){
  angular
  .module('<%= app_name%>')
  .directive('navigationDirective', navigationDirective);

function navigationDirective(){
  return{
    restrict: 'EA',
    templateUrl: '/common/directives/navigation/client.navigation.template.view.html',
    controller: 'navigationCtrl as vm'
  };
}
})();