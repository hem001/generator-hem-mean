(function(){
   angular
   .module('<%= app_name%>')
   .controller('navigationCtrl', navigationCtrl)
   ;


   navigationCtrl.$inject = ['$location', 'authenticationService'];

   function navigationCtrl($location, authenticationService){
    var vm = this;
    vm.isLoggedIn = authenticationService.isLoggedIn();
    vm.currentUser = authenticationService.currentUser();
    
    vm.logout = function(){
        authenticationService.logout();
        vm.isLoggedIn = authenticationService.isLoggedIn();
        $location.path('/');
    };

   }

})();
