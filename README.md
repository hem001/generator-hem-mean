# generator-hem-mean

This is a boilerplate for MEAN full stack app using Yeoman Generator.
Structure as follows:
```
app-name
	app_api
		models
		controllers
		config
		routes
	app_client
		common
			directives
			factories
			filters
			services
		controllers
		views
		index.html
		routes.js
	public
		stylesheets
		app-name.min.js
	.env
	.gitignore
	nodemon.json
	and so on
```
### Before using :
1. install Node, Npm and YEOMAN generator
2. clone the repo
3. 'npm link' to make hem-mean available locally,
### USAGE:
1. 'yo hem-mean'
2. enter app_name
3. enter MongoData base name
4. 'npm install' //install all the dependencies

### Running the application:
1. run MongoD server locally
2. npm start or nodemon

### To register a user for the first time:**
	1. open app_client/controllers/client.authorisation.controller.js
	2. in the function vm.register(),
		comment the line : 
			vm.isLoggedIn = authenticationService.isLoggedIn();
		and replace it with :
			vm.isLoggedIn = true;
	3. undo the changes once you can log in. After initial log in you can register more users.
		
	



